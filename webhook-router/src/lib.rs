// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![deny(missing_docs)]
// XXX(rust-1.66)]
#![allow(clippy::uninlined_format_args)]

//! Verify, route, and write webhooks to a location on disk.
//!
//! This crate handles incoming webhook contents and using HTTP headers and the contents of the
//! webhook itself to determine whether it is a valid webhook or not and route it to a file on disk
//! with a classification as to what kind of webhook it is.

mod config;
mod handler;
mod router;

pub use config::Config;
pub use router::Router;

#[cfg(test)]
mod test_utils;
