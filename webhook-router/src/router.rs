// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::path::PathBuf;
use std::sync::RwLock;

use http::{HeaderMap, Method, Request, Response, StatusCode};
use log::{debug, error};
use serde_json::Value;

use crate::config::{Config, ConfigError};

/// A router for the Iron framework.
///
/// Drops JSON objects which are received via `POST` into a directory using the current date as the
/// filename.
pub struct Router {
    /// The path to the configuration file.
    path: PathBuf,
    /// The configuration for sorting objects based on their type.
    config: RwLock<Config>,
    /// The secrets for various projects.
    secrets: RwLock<Value>,
}

impl Router {
    /// Create a new router for a path.
    pub fn new<P: Into<PathBuf>>(path: P) -> Result<Self, ConfigError> {
        let path = path.into();
        let config = Config::from_path(&path)?;

        Ok(Self {
            path,

            secrets: RwLock::new(config.secrets()?),
            config: RwLock::new(config),
        })
    }

    /// Reload the filename.
    fn reload(&self) -> http::Result<Response<String>> {
        match Config::from_path(&self.path) {
            Ok(config) => {
                {
                    let mut inner_config = self
                        .config
                        .write()
                        .expect("expected to be able to get a write lock on the configuration");

                    *inner_config = config;
                }

                self.reload_secrets()
            },
            Err(err) => {
                error!("failed to load configuration: {:?}", err);

                Response::builder()
                    .status(StatusCode::NOT_ACCEPTABLE)
                    .body(format!("{:?}", err))
            },
        }
    }

    /// Reload secrets.
    fn reload_secrets(&self) -> http::Result<Response<String>> {
        let config = self
            .config
            .read()
            .expect("expected to be able to get a read lock on the configuration");

        match config.secrets() {
            Ok(secrets) => {
                let mut inner_secrets = self
                    .secrets
                    .write()
                    .expect("expected to be able to get a write lock on the secrets");

                *inner_secrets = secrets;

                Response::builder()
                    .status(StatusCode::OK)
                    .body(String::new())
            },
            Err(err) => {
                error!("failed to load secrets: {:?}", err);

                Response::builder()
                    .status(StatusCode::NOT_ACCEPTABLE)
                    .body(format!("{:?}", err))
            },
        }
    }

    /// Handle an object received over the given path.
    fn handle_impl(
        &self,
        path: &str,
        headers: &HeaderMap,
        data: &[u8],
        object: Value,
    ) -> http::Result<Response<()>> {
        let config = self
            .config
            .read()
            .expect("expected to be able to get a read lock on the configuration");

        if let Some(handler) = config.post_paths.get(path) {
            let secret = {
                let secrets = self
                    .secrets
                    .read()
                    .expect("expected to be able to get a read lock on the secrets");

                handler.lookup_secret(&secrets, &object).map(String::from)
            };

            if !handler.verify(headers, secret.as_ref().map(AsRef::as_ref), data) {
                error!(
                    target: "handler",
                    "failed to verify the a webhook:\nheaders:\n{:?}\ndata:\n{}",
                    headers,
                    String::from_utf8_lossy(data),
                );

                return Response::builder()
                    .status(StatusCode::NOT_ACCEPTABLE)
                    .body(());
            }

            if let Some(kind) = handler.kind(headers, &object) {
                if let Err(err) = handler.write_object(&kind, object.clone()) {
                    error!(
                        target: "handler",
                        "failed to write the {} object {}: {:?}",
                        kind,
                        object,
                        err,
                    );

                    // TODO: Should this return 500 Internal Server Error?
                }
            }

            Response::builder().status(StatusCode::ACCEPTED).body(())
        } else {
            Response::builder().status(StatusCode::NOT_FOUND).body(())
        }
    }

    /// Handle an incoming HTTP request.
    ///
    /// This ends up deserializing the data as JSON if it validates. It is passed in as bytes to
    /// avoid forcing deserialization on the caller.
    pub fn handle(&self, req: &Request<Vec<u8>>) -> Result<Response<String>, http::Error> {
        let path = req.uri().path();

        if path.is_empty() {
            return Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(String::new());
        }

        // Remove the leading slash.
        let path = &path[1..];

        debug!(
            target: "handler",
            "got a {} request at {}",
            req.method(),
            path,
        );

        Ok(match *req.method() {
            Method::PUT => {
                if path == "__reload" {
                    self.reload()?
                } else if path == "__reload_secrets" {
                    self.reload_secrets()?
                } else {
                    Response::builder()
                        .status(StatusCode::NOT_FOUND)
                        .body(String::new())?
                }
            },
            Method::POST => {
                let data = req.body();

                serde_json::from_slice(data)
                    .map(|object| {
                        self.handle_impl(path, req.headers(), data, object)
                            .map(|rsp| rsp.map(|()| String::new()))
                    })
                    .unwrap_or_else(|err| {
                        Response::builder()
                            .status(StatusCode::BAD_REQUEST)
                            .body(format!("{:?}", err))
                    })?
            },
            _ => {
                Response::builder()
                    .status(StatusCode::METHOD_NOT_ALLOWED)
                    .body(String::new())?
            },
        })
    }
}

#[cfg(test)]
mod test {
    use std::ffi::OsStr;
    use std::fs::{self, DirEntry, File, OpenOptions};
    use std::path::Path;

    use http::{Method, Request, StatusCode};
    use serde_json::{json, Value};

    use crate::router::Router;
    use crate::test_utils;

    fn create_router(path: &Path, config: Value, secrets: Value) -> Router {
        let (config_path, _) = test_utils::write_config_secrets(path, config, secrets);
        Router::new(config_path).unwrap()
    }

    fn hook_files(path: &Path) -> Vec<DirEntry> {
        let current_dir = OsStr::new(".");
        let parent_dir = OsStr::new("..");
        fs::read_dir(path)
            .unwrap()
            .map(Result::unwrap)
            .filter(|entry| {
                let file_name = entry.file_name();
                file_name != current_dir && file_name != parent_dir
            })
            .collect()
    }

    #[test]
    fn test_reload() {
        let tempdir = test_utils::create_tempdir();
        let router = create_router(tempdir.path(), json!({}), json!({}));

        // Check that the test endpoint doesn't exist.
        {
            let hook = json!({});
            let req = Request::post("/test")
                .body(serde_json::to_vec(&hook).unwrap())
                .unwrap();
            let rsp = router.handle(&req).unwrap();

            assert_eq!(rsp.status(), StatusCode::NOT_FOUND);
            assert_eq!(rsp.body(), "");
        }

        // Rewrite the configuration.
        let test_path = tempdir.path().join("test");
        fs::create_dir(&test_path).unwrap();
        {
            let mut fout = OpenOptions::new().write(true).open(&router.path).unwrap();
            let config = json!({
                "post_paths": {
                    "test": {
                        "path": test_path.to_str().unwrap(),
                        "filters": [],
                    },
                },
            });
            serde_json::to_writer(&mut fout, &config).unwrap();
        }

        // Reload the configuration.
        {
            let req = Request::put("/__reload").body(Vec::new()).unwrap();
            let rsp = router.handle(&req).unwrap();

            assert_eq!(rsp.status(), StatusCode::OK);
            assert_eq!(rsp.body(), "");
        }

        // Retry the endpoint with success.
        {
            let hook = json!({});
            let req = Request::post("/test")
                .body(serde_json::to_vec(&hook).unwrap())
                .unwrap();
            let rsp = router.handle(&req).unwrap();

            assert_eq!(rsp.status(), StatusCode::ACCEPTED);
            assert_eq!(rsp.body(), "");
        }

        // Without filters, the hook directory should be empty.
        {
            let hook_files = hook_files(&test_path);
            assert!(hook_files.is_empty());
        }
    }

    #[test]
    fn test_reload_error() {
        let router = {
            let tempdir = test_utils::create_tempdir();
            create_router(tempdir.path(), json!({}), json!({}))
        };
        let req = Request::put("/__reload").body(Vec::new()).unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::NOT_ACCEPTABLE);
        assert!(
            rsp.body().contains("Read {"),
            "Error response did not match: {}",
            rsp.body(),
        );
    }

    #[test]
    fn test_reload_broken_secrets() {
        let tempdir = test_utils::create_tempdir();
        let router = create_router(tempdir.path(), json!({}), json!({}));

        // Rewrite the secrets.
        {
            let config = router
                .config
                .read()
                .expect("expected to be able to get a read lock on the configuration");
            let secrets_path = config.secrets_path().unwrap();
            File::create(secrets_path).unwrap();
        }

        // Reload the configuration.
        let req = Request::put("/__reload").body(Vec::new()).unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::NOT_ACCEPTABLE);
        assert!(
            rsp.body().contains("while parsing a value"),
            "Error response did not match: {}",
            rsp.body(),
        );
    }

    #[test]
    fn test_reload_secrets_error() {
        let router = {
            let tempdir = test_utils::create_tempdir();
            create_router(tempdir.path(), json!({}), json!({}))
        };
        let req = Request::put("/__reload_secrets").body(Vec::new()).unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::NOT_ACCEPTABLE);
        assert!(
            rsp.body().contains("Read {"),
            "Error response did not match: {}",
            rsp.body(),
        );
    }

    #[test]
    fn test_invalid_put() {
        let tempdir = test_utils::create_tempdir();
        let router = create_router(tempdir.path(), json!({}), json!({}));
        let req = Request::put("/not_an_endpoint").body(Vec::new()).unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::NOT_FOUND);
        assert_eq!(rsp.body(), "");
    }

    #[test]
    fn test_invalid_methods() {
        let tempdir = test_utils::create_tempdir();
        let router = create_router(tempdir.path(), json!({}), json!({}));
        let invalid_methods = [
            Method::GET,
            // Method::POST,
            // Method::PUT,
            Method::DELETE,
            Method::HEAD,
            Method::OPTIONS,
            Method::CONNECT,
            Method::PATCH,
            Method::TRACE,
        ];

        for invalid_method in invalid_methods.iter().cloned() {
            let mut req = Request::new(Vec::new());
            *req.method_mut() = invalid_method;
            let rsp = router.handle(&req).unwrap();

            assert_eq!(rsp.status(), StatusCode::METHOD_NOT_ALLOWED);
            assert_eq!(rsp.body(), "");
        }
    }

    #[test]
    fn test_bad_hook() {
        let tempdir = test_utils::create_tempdir();
        let router = create_router(tempdir.path(), json!({}), json!({}));

        let req = Request::post("/test").body(Vec::new()).unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::BAD_REQUEST);
        assert!(
            rsp.body().contains("while parsing a value"),
            "Error response did not match: {}",
            rsp.body(),
        );
    }

    #[test]
    fn test_write_hook_error() {
        let tempdir = test_utils::create_tempdir();
        let test_path = tempdir.path().join("test");
        // Don't create the directory to cause the write error.
        let config = json!({
            "post_paths": {
                "test": {
                    "path": test_path.to_str().unwrap(),
                    "filters": [
                        {
                            "kind": "unknown",
                        },
                    ],
                },
            },
        });
        let router = create_router(tempdir.path(), config, json!({}));

        let hook = json!({});
        let req = Request::post("/test")
            .body(serde_json::to_vec(&hook).unwrap())
            .unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::ACCEPTED);
        assert_eq!(rsp.body(), "");
    }

    #[test]
    fn test_hook() {
        let tempdir = test_utils::create_tempdir();
        let test_path = tempdir.path().join("test");
        fs::create_dir(&test_path).unwrap();
        let config = json!({
            "post_paths": {
                "test": {
                    "path": test_path.to_str().unwrap(),
                    "filters": [
                        {
                            "kind": "unknown",
                        },
                    ],
                },
            },
        });
        let router = create_router(tempdir.path(), config, json!({}));

        let hook = json!({});
        let req = Request::post("/test")
            .body(serde_json::to_vec(&hook).unwrap())
            .unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::ACCEPTED);
        assert_eq!(rsp.body(), "");

        let hook_files = hook_files(&test_path);
        assert_eq!(hook_files.len(), 1);

        let path = hook_files[0].path();
        let hook_contents = fs::read_to_string(path).unwrap();
        let actual: Value = serde_json::from_str(&hook_contents).unwrap();

        assert_eq!(
            actual,
            json!({
                "kind": "unknown",
                "data": {},
            }),
        );
    }

    #[test]
    fn test_unverified_hook() {
        let tempdir = test_utils::create_tempdir();
        let test_path = tempdir.path().join("test");
        fs::create_dir(&test_path).unwrap();
        let config = json!({
            "post_paths": {
                "test": {
                    "path": test_path.to_str().unwrap(),
                    "filters": [
                        {
                            "kind": "unknown",
                        },
                    ],
                    "verification": {
                        "secret_key_lookup": "/secret",
                        "verification_header": "X-Verify-Webhook",
                        "compare": {
                            "type": "token",
                        },
                    },
                },
            },
        });
        let secrets = json!({
            "secret": "secret",
        });
        let router = create_router(tempdir.path(), config, secrets);

        let hook = json!({});
        let req = Request::post("/test")
            .body(serde_json::to_vec(&hook).unwrap())
            .unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::NOT_ACCEPTABLE);
        assert_eq!(rsp.body(), "");

        // With verification failing, the hook directory should be empty.
        let hook_files = hook_files(&test_path);
        assert!(hook_files.is_empty());
    }

    #[test]
    fn test_verified_hook() {
        let tempdir = test_utils::create_tempdir();
        let test_path = tempdir.path().join("test");
        fs::create_dir(&test_path).unwrap();
        let config = json!({
            "post_paths": {
                "test": {
                    "path": test_path.to_str().unwrap(),
                    "filters": [
                        {
                            "kind": "unknown",
                        },
                    ],
                    "verification": {
                        "secret_key_lookup": "secret",
                        "verification_header": "X-Verify-Webhook",
                        "compare": {
                            "type": "token",
                        },
                    },
                },
            },
        });
        let secrets = json!({
            "secret": "secret",
        });
        let router = create_router(tempdir.path(), config, secrets);

        let hook = json!({});
        let req = Request::post("/test")
            .header("X-Verify-Webhook", "secret")
            .body(serde_json::to_vec(&hook).unwrap())
            .unwrap();
        let rsp = router.handle(&req).unwrap();

        assert_eq!(rsp.status(), StatusCode::ACCEPTED);
        assert_eq!(rsp.body(), "");

        let hook_files = hook_files(&test_path);
        assert_eq!(hook_files.len(), 1);

        let path = hook_files[0].path();
        let hook_contents = fs::read_to_string(path).unwrap();
        let actual: Value = serde_json::from_str(&hook_contents).unwrap();

        assert_eq!(
            actual,
            json!({
                "kind": "unknown",
                "data": {},
            }),
        );
    }
}
